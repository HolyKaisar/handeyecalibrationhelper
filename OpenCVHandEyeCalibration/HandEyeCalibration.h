#pragma once
#include "Header.h"
#include "Utilities.h"

// OpenCV Hand Eye Calibration
class HandEyeCalibration
{
public:
	HandEyeCalibration();
	~HandEyeCalibration();


	void FindCorrespondences(Mat& cameraMatrix, Mat& distCoeffs);
};

