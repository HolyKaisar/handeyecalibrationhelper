#include "stdafx.h"
#include "Utilities.h"
#include "SingleCameraCalibration.h"

namespace fs = std::filesystem;
enum { DETECTION = 0, CAPTURING = 1, CALIBRATED = 2 };

//#define DEBUG


SingleCameraCalibration::SingleCameraCalibration()
{
}


SingleCameraCalibration::~SingleCameraCalibration()
{
}


bool SingleCameraCalibration::ExecuteCalibrationProcedure(Settings& s)
{
	// Load all input images
	vector<Mat> inputImgs;
	string path = "C://Users//holyk//Downloads//opencv-3.4//samples//data//images"; // Input image folder path
	string ext = ".jpg"; // Input the image type
	LoadInputImages(path, ext, inputImgs);

	// Calculate 
	vector<vector<Point2f> > imagePoints;
	Mat cameraMatrix, distCoeffs;
	Size imageSize;
	int mode = CAPTURING;
	clock_t prevTimestamp = 0;
	const Scalar RED(0, 0, 255), GREEN(0, 255, 0);
	const char ESC_KEY = 27;

	for (Mat& m : inputImgs)
	{
		bool blinkOutput = false; // TODO check what that is
		bool found;

		imageSize = m.size();

		vector<Point2f> pointBuf;
		int chessBoardFlags = CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE | CALIB_CB_FAST_CHECK;

		switch (s.calibrationPattern)
		{
		case Settings::CHESSBOARD:
			found = findChessboardCorners(m, s.boardSize, pointBuf, chessBoardFlags);
			break;
		case Settings::CIRCLES_GRID:
			found = findCirclesGrid(m, s.boardSize, pointBuf);
			break;
		case Settings::ASYMMETRIC_CIRCLES_GRID:
			found = findCirclesGrid(m, s.boardSize, pointBuf, CALIB_CB_ASYMMETRIC_GRID);
			break;
		default:
			found = false;
			break;
		}

		if (found)
		{
			// Improve the found corners' coordinate accuracy for chessboard
			if (s.calibrationPattern == Settings::CHESSBOARD)
			{
				Mat viewGrey;
				cvtColor(m, viewGrey, COLOR_BGR2GRAY);
				cornerSubPix(viewGrey, pointBuf, Size(11, 11), Size(-1, -1),
					TermCriteria(TermCriteria::EPS + TermCriteria::COUNT, 30, 0.1));
			}
			imagePoints.push_back(pointBuf);

			// TODO For camera only mode, check the 3.4 sample code for more details

			// Draw the corners
			drawChessboardCorners(m, s.boardSize, Mat(pointBuf), found);
		}

		//----------------------------- Output Text ------------------------------------------------
		//! [output_text]
		string msg = (mode == CAPTURING) ? "100/100" :
			mode == CALIBRATED ? "Calibrated" : "Press 'g' to start";
		int baseLine = 0;
		Size textSize = getTextSize(msg, 1, 1, 1, &baseLine);
		Point textOrigin(m.cols - 2 * textSize.width - 10, m.rows - 2 * baseLine - 10);

		if (mode == CAPTURING)
		{
			if (s.showUndistorsed)
				msg = format("%d/%d Undist", (int)imagePoints.size(), s.nrFrames);
			else
				msg = format("%d/%d", (int)imagePoints.size(), s.nrFrames);
		}

		putText(m, msg, textOrigin, 1, 1, mode == CALIBRATED ? GREEN : RED);

		if (blinkOutput)
			bitwise_not(m, m);
	}

	if (!imagePoints.empty()) // Run calibration after loading all images
	{
		RunCalibrationAndSave(s, imageSize, cameraMatrix, distCoeffs, imagePoints);
	}


	// -----------------------Show the undistorted image for the image list ------------------------
	//! [show_results]
	if (s.inputType == Settings::IMAGE_LIST && s.showUndistorsed)
	{
		Mat view, rview, map1, map2;

		if (s.useFisheye)
		{
			Mat newCamMat;
			fisheye::estimateNewCameraMatrixForUndistortRectify(cameraMatrix, distCoeffs, imageSize,
				Matx33d::eye(), newCamMat, 1);
			fisheye::initUndistortRectifyMap(cameraMatrix, distCoeffs, Matx33d::eye(), newCamMat, imageSize,
				CV_16SC2, map1, map2);
		}
		else
		{
			initUndistortRectifyMap(
				cameraMatrix, distCoeffs, Mat(),
				getOptimalNewCameraMatrix(cameraMatrix, distCoeffs, imageSize, 1, imageSize, 0), imageSize,
				CV_16SC2, map1, map2);
		}

		for (size_t i = 0; i < inputImgs.size(); i++)
		{
			view = inputImgs[i];
			if (view.empty()) continue;
			remap(view, rview, map1, map2, INTER_LINEAR);
			imshow(to_string(i) + " Image View", rview);
			char c = (char)waitKey();
#ifdef DEBUG
			system("pause");
#endif
		}
	}
	return true;
}



bool SingleCameraCalibration::LoadInputImages(const string path, const string ext, vector<Mat>& imgVec, const int imgFormat)
{
	for (const auto & entry : fs::directory_iterator(path))
	{
		string tPath = entry.path().generic_string();
		string tExt = tPath.substr((int)tPath.size() - 4);
		if (tExt == ext)
		{
			Mat tMat = imread(tPath, imgFormat), m;
			imgVec.push_back(m);
			tMat.copyTo(imgVec.back());
		}
	}
	return true;
}

bool SingleCameraCalibration::RunCalibrationAndSave(Settings& s, Size imageSize, Mat& cameraMatrix, Mat& distCoeffs,
	vector<vector<Point2f> > imagePoints)
{
	vector<Mat> rvecs, tvecs;
	vector<float> reprojErrs;
	double totalAvgErr = 0;

	bool ok = RunCalibration(s, imageSize, cameraMatrix, distCoeffs, imagePoints, rvecs, tvecs, reprojErrs,
		totalAvgErr);
	cout << (ok ? "Calibration succeeded" : "Calibration failed")
		<< ". avg re projection error = " << totalAvgErr << endl;

	if (ok)
		SaveCameraParams(s, imageSize, cameraMatrix, distCoeffs, rvecs, tvecs, reprojErrs, imagePoints,
			totalAvgErr);
	return ok;
}


//! [board_corners]
 bool SingleCameraCalibration::RunCalibration(Settings& s, Size& imageSize, Mat& cameraMatrix, Mat& distCoeffs,
	vector<vector<Point2f> > imagePoints, vector<Mat>& rvecs, vector<Mat>& tvecs,
	vector<float>& reprojErrs, double& totalAvgErr)
{
	//! [fixed_aspect]
	cameraMatrix = Mat::eye(3, 3, CV_64F);
	if (s.flag & CALIB_FIX_ASPECT_RATIO)
		cameraMatrix.at<double>(0, 0) = s.aspectRatio;
	//! [fixed_aspect]
	if (s.useFisheye) {
		distCoeffs = Mat::zeros(4, 1, CV_64F);
	}
	else {
		distCoeffs = Mat::zeros(8, 1, CV_64F);
	}

	vector<vector<Point3f> > objectPoints(1);
	CalcBoardCornerPositions(s.boardSize, s.squareSize, objectPoints[0], s.calibrationPattern);

	objectPoints.resize(imagePoints.size(), objectPoints[0]);

	//Find intrinsic and extrinsic camera parameters
	double rms;

	if (s.useFisheye) {
		Mat _rvecs, _tvecs;
		rms = fisheye::calibrate(objectPoints, imagePoints, imageSize, cameraMatrix, distCoeffs, _rvecs,
			_tvecs, s.flag);

		rvecs.reserve(_rvecs.rows);
		tvecs.reserve(_tvecs.rows);
		for (int i = 0; i < int(objectPoints.size()); i++) {
			rvecs.push_back(_rvecs.row(i));
			tvecs.push_back(_tvecs.row(i));
		}
	}
	else {
		rms = calibrateCamera(objectPoints, imagePoints, imageSize, cameraMatrix, distCoeffs, rvecs, tvecs,
			s.flag);
	}

	cout << "Re-projection error reported by calibrateCamera: " << rms << endl;

	bool ok = checkRange(cameraMatrix) && checkRange(distCoeffs);

	totalAvgErr = ComputeReprojectionErrors(objectPoints, imagePoints, rvecs, tvecs, cameraMatrix,
		distCoeffs, reprojErrs, s.useFisheye);

	if (ok)
	{
		m_intrinsicMat = cameraMatrix;
		m_distCoeffs = distCoeffs;
	}
	return ok;
}

// Print camera parameters to the output file
 void SingleCameraCalibration::SaveCameraParams(Settings& s, Size& imageSize, Mat& cameraMatrix, Mat& distCoeffs,
	 const vector<Mat>& rvecs, const vector<Mat>& tvecs,
	 const vector<float>& reprojErrs, const vector<vector<Point2f> >& imagePoints,
	 double totalAvgErr)
 {
	 FileStorage fs(s.outputFileName, FileStorage::WRITE);

	 time_t tm;
	 time(&tm);
	 struct tm *t2 = localtime(&tm);
	 char buf[1024];
	 strftime(buf, sizeof(buf), "%c", t2);

	 fs << "calibration_time" << buf;

	 if (!rvecs.empty() || !reprojErrs.empty())
		 fs << "nr_of_frames" << (int)std::max(rvecs.size(), reprojErrs.size());
	 fs << "image_width" << imageSize.width;
	 fs << "image_height" << imageSize.height;
	 fs << "board_width" << s.boardSize.width;
	 fs << "board_height" << s.boardSize.height;
	 fs << "square_size" << s.squareSize;

	 if (s.flag & CALIB_FIX_ASPECT_RATIO)
		 fs << "fix_aspect_ratio" << s.aspectRatio;

	 if (s.flag)
	 {
		 std::stringstream flagsStringStream;
		 if (s.useFisheye)
		 {
			 flagsStringStream << "flags:"
				 << (s.flag & fisheye::CALIB_FIX_SKEW ? " +fix_skew" : "")
				 << (s.flag & fisheye::CALIB_FIX_K1 ? " +fix_k1" : "")
				 << (s.flag & fisheye::CALIB_FIX_K2 ? " +fix_k2" : "")
				 << (s.flag & fisheye::CALIB_FIX_K3 ? " +fix_k3" : "")
				 << (s.flag & fisheye::CALIB_FIX_K4 ? " +fix_k4" : "")
				 << (s.flag & fisheye::CALIB_RECOMPUTE_EXTRINSIC ? " +recompute_extrinsic" : "");
		 }
		 else
		 {
			 flagsStringStream << "flags:"
				 << (s.flag & CALIB_USE_INTRINSIC_GUESS ? " +use_intrinsic_guess" : "")
				 << (s.flag & CALIB_FIX_ASPECT_RATIO ? " +fix_aspectRatio" : "")
				 << (s.flag & CALIB_FIX_PRINCIPAL_POINT ? " +fix_principal_point" : "")
				 << (s.flag & CALIB_ZERO_TANGENT_DIST ? " +zero_tangent_dist" : "")
				 << (s.flag & CALIB_FIX_K1 ? " +fix_k1" : "")
				 << (s.flag & CALIB_FIX_K2 ? " +fix_k2" : "")
				 << (s.flag & CALIB_FIX_K3 ? " +fix_k3" : "")
				 << (s.flag & CALIB_FIX_K4 ? " +fix_k4" : "")
				 << (s.flag & CALIB_FIX_K5 ? " +fix_k5" : "");
		 }
		 fs.writeComment(flagsStringStream.str());
	 }

	 fs << "flags" << s.flag;

	 fs << "fisheye_model" << s.useFisheye;

	 fs << "camera_matrix" << cameraMatrix;
	 fs << "distortion_coefficients" << distCoeffs;

	 fs << "avg_reprojection_error" << totalAvgErr;
	 if (s.writeExtrinsics && !reprojErrs.empty())
		 fs << "per_view_reprojection_errors" << Mat(reprojErrs);

	 if (s.writeExtrinsics && !rvecs.empty() && !tvecs.empty())
	 {
		 CV_Assert(rvecs[0].type() == tvecs[0].type());
		 Mat bigmat((int)rvecs.size(), 6, CV_MAKETYPE(rvecs[0].type(), 1));
		 bool needReshapeR = rvecs[0].depth() != 1 ? true : false;
		 bool needReshapeT = tvecs[0].depth() != 1 ? true : false;

		 for (size_t i = 0; i < rvecs.size(); i++)
		 {
			 Mat r = bigmat(Range(int(i), int(i + 1)), Range(0, 3));
			 Mat t = bigmat(Range(int(i), int(i + 1)), Range(3, 6));

			 if (needReshapeR)
				 rvecs[i].reshape(1, 1).copyTo(r);
			 else
			 {
				 //*.t() is MatExpr (not Mat) so we can use assignment operator
				 CV_Assert(rvecs[i].rows == 3 && rvecs[i].cols == 1);
				 r = rvecs[i].t();
			 }

			 if (needReshapeT)
				 tvecs[i].reshape(1, 1).copyTo(t);
			 else
			 {
				 CV_Assert(tvecs[i].rows == 3 && tvecs[i].cols == 1);
				 t = tvecs[i].t();
			 }
		 }
		 fs.writeComment("a set of 6-tuples (rotation vector + translation vector) for each view");
		 fs << "extrinsic_parameters" << bigmat;
	 }

	 if (s.writePoints && !imagePoints.empty())
	 {
		 Mat imagePtMat((int)imagePoints.size(), (int)imagePoints[0].size(), CV_32FC2);
		 for (size_t i = 0; i < imagePoints.size(); i++)
		 {
			 Mat r = imagePtMat.row(int(i)).reshape(2, imagePtMat.cols);
			 Mat imgpti(imagePoints[i]);
			 imgpti.copyTo(r);
		 }
		 fs << "image_points" << imagePtMat;
	 }
 }


void SingleCameraCalibration::CalcBoardCornerPositions(Size boardSize, float squareSize, vector<Point3f>& corners,
	Settings::Pattern patternType /*= Settings::CHESSBOARD*/)
{
	corners.clear();

	switch (patternType)
	{
	case Settings::CHESSBOARD:
	case Settings::CIRCLES_GRID:
		for (int i = 0; i < boardSize.height; ++i)
			for (int j = 0; j < boardSize.width; ++j)
				corners.push_back(Point3f(j*squareSize, i*squareSize, 0));
		break;

	case Settings::ASYMMETRIC_CIRCLES_GRID:
		for (int i = 0; i < boardSize.height; i++)
			for (int j = 0; j < boardSize.width; j++)
				corners.push_back(Point3f((2 * j + i % 2)*squareSize, i*squareSize, 0));
		break;
	default:
		break;
	}
}


double SingleCameraCalibration::ComputeReprojectionErrors(const vector<vector<Point3f> >& objectPoints,
	const vector<vector<Point2f> >& imagePoints,
	const vector<Mat>& rvecs, const vector<Mat>& tvecs,
	const Mat& cameraMatrix, const Mat& distCoeffs,
	vector<float>& perViewErrors, bool fisheye)
{
	vector<Point2f> imagePoints2;
	size_t totalPoints = 0;
	double totalErr = 0, err;
	perViewErrors.resize(objectPoints.size());

	for (size_t i = 0; i < objectPoints.size(); ++i)
	{
		if (fisheye)
		{
			fisheye::projectPoints(objectPoints[i], imagePoints2, rvecs[i], tvecs[i], cameraMatrix,
				distCoeffs);
		}
		else
		{
			projectPoints(objectPoints[i], rvecs[i], tvecs[i], cameraMatrix, distCoeffs, imagePoints2);
		}
		err = norm(imagePoints[i], imagePoints2, NORM_L2);

		size_t n = objectPoints[i].size();
		perViewErrors[i] = (float)std::sqrt(err*err / n);
		totalErr += err * err;
		totalPoints += n;
	}

	return std::sqrt(totalErr / totalPoints);
}