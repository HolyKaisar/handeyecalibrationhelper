#pragma once

#include "Header.h"
#include "Utilities.h"

class SingleCameraCalibration
{
public:
	SingleCameraCalibration();
	~SingleCameraCalibration();

	bool ExecuteCalibrationProcedure(Settings& s);

	bool LoadInputImages(const string path, const string ext, vector<Mat>& imgVec, const int imgFormat = IMREAD_COLOR);

	bool RunCalibrationAndSave(Settings& s, Size imageSize, Mat& cameraMatrix, Mat& distCoeffs,
		vector<vector<Point2f> > imagePoints);

	bool RunCalibration(Settings& s, Size& imageSize, Mat& cameraMatrix, Mat& distCoeffs,
		vector<vector<Point2f> > imagePoints, vector<Mat>& rvecs, vector<Mat>& tvecs,
		vector<float>& reprojErrs, double& totalAvgErr);

	void SaveCameraParams(Settings& s, Size& imageSize, Mat& cameraMatrix, Mat& distCoeffs,
		const vector<Mat>& rvecs, const vector<Mat>& tvecs,
		const vector<float>& reprojErrs, const vector<vector<Point2f> >& imagePoints,
		double totalAvgErr);

	void CalcBoardCornerPositions(Size boardSize, float squareSize, vector<Point3f>& corners,
		Settings::Pattern patternType /*= Settings::CHESSBOARD*/);

	double ComputeReprojectionErrors(const vector<vector<Point3f> >& objectPoints,
		const vector<vector<Point2f> >& imagePoints,
		const vector<Mat>& rvecs, const vector<Mat>& tvecs,
		const Mat& cameraMatrix, const Mat& distCoeffs,
		vector<float>& perViewErrors, bool fisheye);

public:
	Mat m_intrinsicMat;
	Mat m_distCoeffs;
};

