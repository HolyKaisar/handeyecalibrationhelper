#pragma once

#include "Header.h"
#include "pxcsensemanager.h"
#include "pxcmetadata.h"
#include "util_render.h"
//#include "pxctrackerutils.h"
//#include "pxctracker.h"
#include "pxcprojection.h"

using namespace std;

// Real Sense Constants
const int REAL_SENSE_RGB_WIDTH = 1920;
const int REAL_SENSE_RGB_HEIGHT = 1080;
const int REAL_SENSE_DEPTH_WIDTH = 640;
const int REAL_SENSE_DEPTH_HEIGHT = 480;

// Real Sense F200 Depth Camera Calibration (640 * 480)
const double depth_cam_fx = 4.7023384138596424e+02;
const double depth_cam_fy = 4.6717960854172998e+02;
const double depth_cam_cx = 3.2904535963105388e+02;
const double depth_cam_cy = 2.5196818592761520e+02;

class RSStreamer: public PXCSenseManager::Handler
{
	struct {
		int module;
		int device;
		int stream[PXCCapture::STREAM_LIMIT];
		void Clear() {
			for (int i = 0; i < PXCCapture::STREAM_LIMIT; i++) stream[i] = -1;
		}
	}g_devices_config;

	struct DeviceInfo {
		pxcUID Uid;
		wstring name;
	};

public:
	RSStreamer(unsigned int rgbMapWidth = REAL_SENSE_RGB_WIDTH,
		unsigned int rgbMapHeight = REAL_SENSE_RGB_HEIGHT,
		unsigned int depthMapWidth = REAL_SENSE_DEPTH_WIDTH,
		unsigned int depthMapHeight = REAL_SENSE_DEPTH_HEIGHT);

	~RSStreamer();

	int Initialization();

	bool Update();

	// Setters and Getters
	char* GetColorRaw() { return m_pColorData; }
	short* GetDepthRaw() { return m_pDepthData; }
	short* GetIRRaw() { return m_pIRData; }

	int GetIRWidth() { return REAL_SENSE_DEPTH_WIDTH; }
	int GetIRHeight() { return REAL_SENSE_DEPTH_HEIGHT; }
	int GetColorWidth() { return REAL_SENSE_RGB_WIDTH; }
	int GetColorHeight() { return REAL_SENSE_RGB_HEIGHT; }
	int GetMaxIRValue() { return m_uMaxIRValue; }

private:
	PXCSession * m_pSession = nullptr;
	PXCCapture* m_pCapture = nullptr;
	PXCProjection *m_pProjection = nullptr;
	PXCCapture::Device* m_pDevice = nullptr;

	PXCSenseManager* m_pSenseManager = nullptr;

	unsigned int m_uDepthMapWidth;
	unsigned int m_uDepthMapHeight;
	unsigned int m_uRGBMapWidth;
	unsigned int m_uRGBMapHeight;
	unsigned int m_uMaxIRValue;

	static const int IDXM_DEVICE = 0;
	static const int ID_STREAM1X = 22000;
	static const int NPROFILES_MAX = 100;
	static const int ID_DEVICEX = 2100;
	static const int NDEVICES_MAX = 100;

	map<int, DeviceInfo> m_mapDevices;

	char*  m_pColorData = nullptr;
	short* m_pDepthData = nullptr;
	short*  m_pIRData = nullptr;

	// TODO Debug Code
	UtilRender* m_pColorRender = nullptr;
	UtilRender* m_pDepthRender = nullptr;
	UtilRender* m_pIRRender = nullptr;
};
