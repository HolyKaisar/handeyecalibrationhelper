#pragma once
#include <iostream>
#include <algorithm>
#include <limits>
#include <cmath>
#include <cstring>
#include <time.h>
#include <fileSystem> // C++17 feature
#include <functional>

#include <vector>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>

#include <windows.h>

using namespace std;

#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/calib3d.hpp>

using namespace cv;
