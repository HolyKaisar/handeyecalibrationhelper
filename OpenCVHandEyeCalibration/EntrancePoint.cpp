#include "stdafx.h"
#include "Header.h"
#include "SingleCameraCalibration.h"
#include "MultiSensorCalibration.h"
#include "RSStreamer.h"
#include "IObjectTracking.h"
#include "Utilities.h"
//#pragma comment(lib, "TrackingDLL.lib")


// First step, calibrate the RGB camera or calibrate the stereo cameras (e.g. depth sensor)
// Second step, perform the Hand-Eye calibration, this step includes three major steps:
//		1. Get a series camera poses T_cam (e.g. in our experiment, a series RGB camera poses and convert to the depth camera poses);
//		2. Get a series robot arms poses T_rob (e.g. in our experiment, T_rob is actually the poses of HMD, which is provided by Vicon
//		3. Estimated AX = XB, where X is the relative transformation that transforms from T_cam space to T_rob or vice verse
enum Step{ SingleCalib = 0, StereoCalib = 1, HandEyeCalib = 2 };

int main()
{
	int calibStep = SingleCalib;

	// Setting Section
	Settings s;
	s.aspectRatio = 1;
	s.boardSize = cv::Size(9, 6);
	s.calibrationPattern = Settings::CHESSBOARD;
	s.calibFixPrincipalPoint = 1;
	s.calibZeroTangentDist = 1;
	s.cameraID = 0;
	s.delay = 100;
	s.fixK1 = 0;
	s.fixK2 = 0;
	s.fixK3 = 0;
	s.fixK4 = 1;
	s.fixK5 = 1;
	s.flag = 0;
	s.flipVertical = false;
	s.goodInput = true;
	s.nrFrames = 14;
	s.showUndistorsed = true;
	s.outputFileName = "out_camera_calib.xml";
	s.squareSize = 50;
	s.useFisheye = false;
	s.inputType = Settings::IMAGE_LIST;
	s.writeExtrinsics = true;

	/*Camera Calibration, it yields intrinsic and extrinsic matrix*/
	// Calibrate the Camera first, this step requires a pre-captured chessboard images to
	// calculate the extrinsic parameters and undistortion parameters for the camera.
	SingleCameraCalibration scc;
	scc.ExecuteCalibrationProcedure(s);



	/*Camera Calibration, acquire chessboard data and Vicon reading*/
	RSStreamer* pRealSense = new RSStreamer();
	pRealSense->Initialization();

	// Initialize Vicon
	Tracking::IObjectTracking* pViconTracker = nullptr;
	pViconTracker = Tracking::ObjectTrackingFactory::create();
	pViconTracker->Initialize("Vicon");

	std::cout << pViconTracker->GetInfo().c_str() << std::endl;

	do {
		pViconTracker->ConnectServer();
		std::cout << pViconTracker->GetInfo().c_str() << std::endl;
	} while (!pViconTracker->IsConnected());

	uint16_t ColorWidth, ColorHeight, InfraredWidth, InfraredHeight;
	do
	{
		ColorWidth = pRealSense->GetColorWidth();
		ColorHeight = pRealSense->GetColorHeight();
	} while (ColorWidth == 0 || ColorHeight == 0);

	do
	{
		InfraredWidth = pRealSense->GetIRWidth();
		InfraredHeight = pRealSense->GetIRHeight();
	} while (InfraredWidth == 0 || InfraredHeight == 0);

	vector<Mat> colorPoses, infPoses, hmdPoses, colorImgs;
	char userInput = ' ';
	string hmdName = "HMDExp";
	Mat mHMDTransform_world, mHMDTransformInv_world;
	int sampleCnt = 0;
	while ('q' != (userInput = cv::waitKey(1)))
	{
		bool acquireData = false;
		if (userInput == 'b') acquireData = true;
		if (acquireData)
		{
			// Get HMD Pose
			if (pViconTracker->IsConnected()) {
				pViconTracker->Update();

				cv::Mat tPos = cv::Mat(3, 1, CV_64F);
				cv::Mat tOri = cv::Mat(3, 3, CV_64F);

				Tracking::Pos pos;
				pViconTracker->GetPosition(hmdName, hmdName, pos);
				tPos.at<double>(0, 0) = pos.X;
				tPos.at<double>(0, 1) = pos.Y;
				tPos.at<double>(0, 2) = -pos.Z;

				double pMat[9];
				pViconTracker->GetRotationMat(hmdName, hmdName, pMat);
				tOri = (cv::Mat_<double>(3, 3) << pMat[0], pMat[1], pMat[2], pMat[3],
					pMat[4], pMat[5], pMat[6], pMat[7], pMat[8]);

				tOri.copyTo(mHMDTransform_world(cv::Rect(0, 0, 3, 3)));
				tPos.copyTo(mHMDTransform_world(cv::Rect(3, 0, 1, 3)));
				//mHMDTransformInv_world = mHMDTransform_world.inv();

				hmdPoses.push_back(mHMDTransform_world);
			}
			else
			{
				cout << "The Vicon is not connected. Abort the operation" << endl;
				return 0;
			}

			if(pRealSense->Update());
			{
				/*We don't need ir images now*/
				//short* pIR = pRealSense->GetIRRaw();
				//// Use depth sensor (infrared camera) as master sensor
				//cv::Mat infMat = cv::Mat(InfraredHeight, InfraredWidth, CV_16UC1, pIR);
				//// Convert infrared image to compatible format
				//infMat.convertTo(infMat, CV_32F);
				//double irMax = pRealSense->GetMaxIRValue();
				////infMat = infMat / 65535.f;
				//infMat = infMat / irMax;
				//cv::pow(infMat, 0.32, infMat);
				//infMat = infMat * 256;
				//infMat.convertTo(infMat, CV_8UC1);
				//cv::Mat infDisplayMat;
				//cvtColor(infMat, infDisplayMat, CV_GRAY2BGR);
				//infCands.push_back(infDisplayMat);

				// Process color camera data (as slave sensor)
				char* pColor = pRealSense->GetColorRaw();
				cv::Mat colMat = cv::Mat(ColorHeight, ColorWidth, CV_8UC4, pColor);
				cv::Mat colDisplayMat;
				cvtColor(colMat, colDisplayMat, CV_BGRA2BGR);
				colorImgs.push_back(colDisplayMat);
			}

			if (acquireData)
			{
				sampleCnt++;
				cout << "Total Sample is " << sampleCnt << endl;
			}
		}
	}

	// Calculate the A, and B matrix for AX = XB
	if (colorImgs.size() != hmdPoses.size())
	{
		cout << "Correspondences are not matched!" << endl;
		return 0;
	}

	for (int i = 0; i < colorImgs.size(); i++)
	{

		bool found = false;
		int chessBoardFlags = CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE | CALIB_CB_FAST_CHECK;
		vector<Point2f> pointBuf, imagePoints;
		switch (s.calibrationPattern)
		{
		case Settings::CHESSBOARD:
			found = findChessboardCorners(colorImgs[i], s.boardSize, pointBuf, chessBoardFlags);
			break;
		case Settings::CIRCLES_GRID:
			found = findCirclesGrid(colorImgs[i], s.boardSize, pointBuf);
			break;
		case Settings::ASYMMETRIC_CIRCLES_GRID:
			found = findCirclesGrid(colorImgs[i], s.boardSize, pointBuf, CALIB_CB_ASYMMETRIC_GRID);
			break;
		default:
			found = false;
			break;
		}

		if (found)
		{
			// Improve the found corners' coordinate accuracy for chessboard
			if (s.calibrationPattern == Settings::CHESSBOARD)
			{
				Mat viewGrey;
				cvtColor(colorImgs[i], viewGrey, COLOR_BGR2GRAY);
				cornerSubPix(viewGrey, pointBuf, Size(11, 11), Size(-1, -1),
					TermCriteria(TermCriteria::EPS + TermCriteria::COUNT, 30, 0.1));
			}
			imagePoints = pointBuf;

			// Draw the corners
			drawChessboardCorners(colorImgs[i], s.boardSize, Mat(pointBuf), found);

			vector<vector<Point3f> > objectPoints(1);
			scc.CalcBoardCornerPositions(s.boardSize, s.squareSize, objectPoints[0], s.calibrationPattern);

			Mat rvec, tvec, tmpRot;
			solvePnP(objectPoints, imagePoints, scc.m_intrinsicMat, scc.m_distCoeffs, rvec, tvec);
			
			// Convert to rotation matrix
			Rodrigues(rvec, tmpRot);

			Mat mCameraTransform_world;
			tmpRot.copyTo(mCameraTransform_world(cv::Rect(0, 0, 3, 3)));
			tvec.copyTo(mCameraTransform_world(cv::Rect(3, 0, 1, 3)));
			colorPoses.push_back(mCameraTransform_world);
		}
		else
		{
			cout << "Bad Sample is found, no image points can be found" << endl;
			return 0;
		}

		vector<Mat> camTs, hmdTs;
		for (int i = 1; i < colorPoses.size(); i++)
		{
			camTs.push_back(colorPoses[i].inv() * colorPoses[i - 1]);
			hmdTs.push_back(hmdPoses[i].inv() * hmdPoses[i - 1]);
		}

		// TODO write data to the txt file
		string handEyeMatrixOutput = "hand_eye_matrices.xml";
		FileStorage fs(s.outputFileName, FileStorage::WRITE);
		std::stringstream matrixStringStream;
		if (camTs.size() == hmdTs.size())
		{
			fs << "Camera T Poses";
			for (int i = 0; i < camTs.size(); i++)
			{
				fs << camTs[i];
				fs << "-----------------";
			}

			fs << "HMD T Poses";
			for (int i = 0; i < hmdTs.size(); i++)
			{
				fs << hmdTs[i];
				fs << "-----------------";
			}
		}
	}
	// TODO import Realsense SR300 libary here

	system("pause");

	return 0;
}
