#include "stdafx.h"
#include "RSStreamer.h"

RSStreamer::RSStreamer(unsigned int rgbMapWidth,
	unsigned int rgbMapHeight,
	unsigned int depthMapWidth,
	unsigned int depthMapHeight)
{
	m_uDepthMapWidth = depthMapWidth;
	m_uDepthMapHeight = depthMapHeight;
	m_uRGBMapWidth = rgbMapWidth;
	m_uRGBMapHeight = rgbMapHeight;

	m_pColorData = new char[m_uRGBMapWidth * m_uRGBMapHeight * 4];
	m_pDepthData = new short[m_uDepthMapWidth * m_uDepthMapHeight];
	m_pIRData = new short[m_uDepthMapWidth * m_uDepthMapHeight];

	if (m_pSenseManager) { m_pSenseManager->Release(); m_pSenseManager = nullptr; }

	if (m_pColorRender) { delete m_pColorRender; m_pColorRender = nullptr; }
	if (m_pDepthRender) { delete m_pDepthRender; m_pDepthRender = nullptr; }
	if (m_pIRRender) { delete m_pIRRender; m_pIRRender = nullptr; }

	// Delete the session last
	if (m_pSession) { m_pSession->Release(); m_pSession = nullptr; }
}

RSStreamer::~RSStreamer()
{
	// Release RealSense devices
	if (m_pProjection) { m_pProjection->Release(); m_pProjection = nullptr; }
	if (m_pCapture) { m_pCapture->Release(); m_pCapture = nullptr; }
	if (m_pDevice) { m_pDevice->Release(); m_pDevice = nullptr; }

	if (m_pColorData)
	{
		delete[] m_pColorData;
		m_pColorData = nullptr;
	}
	if (m_pDepthData)
	{
		delete[] m_pDepthData;
		m_pDepthData = nullptr;
	}
	if (m_pIRData)
	{
		delete[] m_pIRData;
		m_pIRData = nullptr;
	}
}

int RSStreamer::Initialization()
{
	if (m_pSession) { m_pSession->Release(); m_pSession = nullptr; }
	m_pSession = PXCSession::CreateInstance();
	if (!m_pSession) {
		MessageBoxW(0, L"Failed to create SDK session", L"SDK Session", MB_ICONEXCLAMATION | MB_OK);
		return 1; // Means depth camera session creation failed
	}

	// Get realsense devices information
	PXCSession::ImplDesc mdesc = {};
	mdesc.group = PXCSession::IMPL_GROUP_SENSOR;
	mdesc.subgroup = PXCSession::IMPL_SUBGROUP_VIDEO_CAPTURE;
	g_devices_config.module = -1;
	g_devices_config.device = -1;
	g_devices_config.Clear();
	m_mapDevices.clear();

	int defaultDevice = -1;
	for (int m = 0;; m++) {
		PXCSession::ImplDesc desc1;
		if (m_pSession->QueryImpl(&mdesc, m, &desc1) < PXC_STATUS_NO_ERROR) break;

		PXCCapture* tmpCapture = 0;
		if (m_pSession->CreateImpl<PXCCapture>(&desc1, &tmpCapture) < PXC_STATUS_NO_ERROR) continue;

		for (int j = 0;; j++) {
			PXCCapture::DeviceInfo dinfo;
			if (tmpCapture->QueryDeviceInfo(j, &dinfo) < PXC_STATUS_NO_ERROR) break;
			int id = 20000 + m * NDEVICES_MAX + j;
			if (defaultDevice < 0) defaultDevice = id;
			m_mapDevices[id].Uid = desc1.iuid;
			m_mapDevices[id].name = dinfo.name;
			if (g_devices_config.module < 0) {
				g_devices_config.module = m;
				g_devices_config.device = j;
			}
		}
		tmpCapture->Release();
	}

	if (m_pCapture) { m_pCapture->Release(); m_pCapture = nullptr; }
	if (m_pSession->CreateImpl<PXCCapture>(m_mapDevices[defaultDevice].Uid, &m_pCapture) < PXC_STATUS_NO_ERROR) {
		MessageBoxW(0, L"Failed to create Capture onStart", L"Capture", MB_ICONEXCLAMATION | MB_OK);
		return 2;
	}
	if (m_pDevice) { m_pDevice->Release(); m_pDevice = nullptr; }
	//m_pDevice = m_pCapture->CreateDevice((ID_DEVICEX - ID_DEVICEX) % NDEVICES_MAX); // TODO may need to provide the functionality of choosing device ability 
	m_pDevice = m_pCapture->CreateDevice(g_devices_config.device); // TODO may need to provide the functionality of choosing device ability 
	if (!m_pDevice) {
		MessageBoxW(0, L"Failed to create RealSense Device", L"Device", MB_ICONEXCLAMATION | MB_OK);
		return 3; // Means depth camera session creation failed
	}
	// Projection Handler
	if (m_pProjection) { m_pProjection->Release(); m_pProjection = nullptr; }
	m_pProjection = m_pDevice->CreateProjection();
	if (!m_pProjection) {
		MessageBoxW(0, L"Failed to create Projection Handler", L"Projection", MB_ICONEXCLAMATION | MB_OK);
		return 4; // Means depth camera session creation failed
	}

	// PXCSenseManager initialization
	if (m_pSenseManager) { m_pSenseManager->Release(); m_pSenseManager = nullptr; }
	m_pSenseManager = PXCSenseManager::CreateInstance();
	if (!m_pSenseManager) {
		MessageBoxW(0, L"Failed to create SenseManager.", L"SenseManager", MB_ICONEXCLAMATION | MB_OK);
		return 5; // Means depth camera PXCSenseManger creation failed
	}

	// Stream types (TODO How can we specify the color and the depth format
	PXCCapture::StreamType COLOR(PXCCapture::STREAM_TYPE_COLOR);
	PXCCapture::StreamType DEPTH(PXCCapture::STREAM_TYPE_DEPTH);
	PXCCapture::StreamType IR(PXCCapture::STREAM_TYPE_IR);
	m_pSenseManager->EnableStream(COLOR, m_uRGBMapWidth, m_uRGBMapHeight, 30);
	m_pSenseManager->EnableStream(DEPTH, m_uDepthMapWidth, m_uDepthMapHeight, 30);
	m_pSenseManager->EnableStream(IR, m_uDepthMapWidth, m_uDepthMapHeight, 30); // TODO whats the res for IR??????

	if (m_pSenseManager->Init(this) < PXC_STATUS_NO_ERROR) { // sm->Init(this)
		MessageBoxW(0, L"Failed to initialize the SenseManager in DepthCamera Class.", L"SenseManager", MB_ICONEXCLAMATION | MB_OK);
		return 6; // Means depth camera sSenseManger initialization failed
	}

	// TODO Debug code
	if (m_pColorRender) { delete m_pColorRender; m_pColorRender = nullptr; }
	m_pColorRender = new UtilRender(const_cast<wchar_t*>(L"COLOR STREAM"));
	if (m_pDepthRender) { delete m_pDepthRender; m_pDepthRender = nullptr; }
	m_pDepthRender = new UtilRender(const_cast<wchar_t*>(L"DEPTH STREAM"));
	if (m_pIRRender) { delete m_pIRRender; m_pIRRender = nullptr; }
	m_pIRRender = new UtilRender(const_cast<wchar_t*>(L"IR STREAM"));
}

bool RSStreamer::Update()
{
	if (!m_pSenseManager) return false;

	pxcStatus sts = m_pSenseManager->AcquireFrame(true); // Sync or not
	if (sts < PXC_STATUS_NO_ERROR /*&& sts != PXC_STATUS_DEVICE_LOST*/) {
		//MessageBoxW(0, L"SenseManager failed to acquireFrame in DepthCamera.", L"SenseManager", MB_ICONEXCLAMATION | MB_OK);
		//m_bRunning = false;
		return false;
	}

	PXCCapture::Sample *sample = m_pSenseManager->QuerySample();
	if (!sample) return false;
	PXCImage::ImageInfo info = {};
	PXCImage* image = sample->color;
	PXCImage* depth = sample->depth;
	PXCImage* ir = sample->ir;

	if (!image || !depth || !ir) return false;

	// Color Stream
	//if (image && !m_pColorRender->RenderFrame(image)) {
	//	MessageBoxW(0, L"Color Stream Render Failed in DepthCamera.", L"ColorRendering", MB_ICONEXCLAMATION | MB_OK);
	//	return false;
	//}
	PXCImage::ImageData data;
	image->AcquireAccess(PXCImage::ACCESS_READ, PXCImage::PIXEL_FORMAT_RGB32, &data);
	char *tmpRGBData = (char*)data.planes[0];
	memcpy(m_pColorData, tmpRGBData, REAL_SENSE_RGB_HEIGHT * REAL_SENSE_RGB_WIDTH * 4 * sizeof(char));
	image->ReleaseAccess(&data);

	// Depth Stream
	//if (depth && !m_pDepthRender->RenderFrame(depth)) {
	//	MessageBoxW(0, L"Depth Stream Render Failed in DepthCamera.", L"DepthRendering", MB_ICONEXCLAMATION | MB_OK);
	//	return false;
	//}
	depth->AcquireAccess(PXCImage::ACCESS_READ, PXCImage::PIXEL_FORMAT_DEPTH, &data);
	short *tmpDepthData = (short*)data.planes[0];
	for (int i = 0; i < m_uDepthMapHeight * m_uDepthMapWidth; i++) m_pDepthData[i] = tmpDepthData[i];
	depth->ReleaseAccess(&data);

	// IR Stream
	//if (ir && !m_pIRRender->RenderFrame(ir)) {
	//	MessageBoxW(0, L"IR Stream Render Failed in DepthCamera.", L"IRRendering", MB_ICONEXCLAMATION | MB_OK);
	//	return false;
	//}
	ir->AcquireAccess(PXCImage::ACCESS_READ, PXCImage::PIXEL_FORMAT_Y16, &data);
	short *tmpirData = (short*)data.planes[0];
	m_uMaxIRValue = 0;
	for (int i = 0; i < m_uDepthMapHeight * m_uDepthMapWidth; i++)
	{
		m_pIRData[i] = tmpirData[i];
		m_uMaxIRValue = max(m_uMaxIRValue, (unsigned int)m_pIRData[i]);
	}
	ir->ReleaseAccess(&data);

	m_pSenseManager->ReleaseFrame();
	return true;
}